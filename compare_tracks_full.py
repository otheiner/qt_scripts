# This script compares two files with track parameters. It does difference of the track parameters (eta, phi)
# before and after optimisation step. Files have to be in following format (each line is one track):
# eventNumber track.index() layerID isEntrySurface track.eta() track.phi() extrap.eta() extrap.phi() track.pt()
# Extrapolated parameters is intersection with given surface/layer


########## Default input and output text files definitions ##########
#Default input file to compare to some other file specified as an input 
reference_file='sample_files/test_input_reference.txt'
#Name of the output file which contains fractions of missing hits in individual calo layers
file_missing_hits = './missing_hits.txt'
#Name of the output file which contains distances between extrapolated positions with default and some custom settings in individual layers
file_comparison_precision = './comparison_precision.txt'

with open(file_missing_hits, 'w') as output_1:
    output_1.write("Layer   fracion_of_hits_present")
output_1.close()

with open(file_comparison_precision, 'w') as output_2:
    output_2.write("Layer  eta-phi_dist")
output_2.close()


########## Parsing input inline arguments ##########
import sys
if len(sys.argv) == 2:
	#Comparison with respect to standardised reference file
	file1 = sys.argv[1]
	file2 = reference_file
elif len(sys.argv) == 3:
	#Comparison between two arbitrary files
	file1 = sys.argv[1]
	file2 = sys.argv[2]
else:
	print("Wrong number of arguments!")
	sys.exit()
 

########## Definition of the main function which does the comparisons in given selected calorimeter layer ##########
def compare_layer(selectedLayer):
		import ROOT
    arr1=[]
    dict1={}
    hist_eta = ROOT.TH1F("delta_eta","Difference of eta before and after optimization; difference eta; events",1000,-0.008,0.008)
    hist_phi = ROOT.TH1F("delta_phi","Difference of phi before and after optimization; difference phi; events",1000,-0.008,0.008)
    hist_eta_abs = ROOT.TH1F("abs_delta_eta","Absolute value of difference of eta before and after optimization; abs(difference eta); events",1000,-0.008,0.008)
    hist_eta_phi_distance = ROOT.TH1F("eta_phi_distance","Distance in eta-phi plane before and after optimization; distance eta-phi; events",500,0,0.000)
    hist_phi_abs = ROOT.TH1F("abs_delta_eta","Absolute value of difference of phi before and after optimization; abs(difference phi); events",1000,-0.008,0.008)
    hist_eta_2D = ROOT.TH2F("delta_eta_2D","Difference of eta before and after optimization vs. track pT; difference eta; p_T [GeV]",100,-0.005,0.005, 100, 0, 20)
    hist_phi_2D = ROOT.TH2F("delta_phi_2D","Difference of eta before and after optimization vs. track pT; difference phi; p_T [GeV]",100,-0.005,0.005, 100, 0, 20)
    hist_eta_pt = ROOT.TH2F("missing_eta_pt","Eta and pt of missing tracks; eta; p_T [GeV]",100,-3,3, 100, 0, 50)
    hist_phi_pt = ROOT.TH2F("missing_phi_pt","phi and pt of missing tracks; phi; p_T [GeV]",100,-4,4, 100, 0, 50)
    hist_phi_eta = ROOT.TH2F("missing_phi_eta","phi and pt of missing tracks; phi; eta",100,-4,4, 100, -3, 3)
    entrySurface = '1'
    selectedSurface = str(selectedLayer) + 'x' + entrySurface
    
    with open(file1,'r') as file:
        
        # Skip the first line with labels
        next(file)
        for line in file:
            evtNum = line.split()[0]
            index = line.split()[1]
            layer = line.split()[2] + "x" + line.split()[3]
            track_eta = line.split()[4]
            track_phi = line.split()[5]
            extrap_eta = line.split()[6]
            extrap_phi = line.split()[7]
            pt = line.split()[8]

            if layer == selectedSurface:
                arr1.append([evtNum + "x" + index, layer, track_eta, track_phi, extrap_eta, extrap_phi, pt])

    file.close()
    print ("Loaded file 1 to the array.")

    with open(file2, 'r') as file:
        
        # Skip the first line with labels
        next(file)
        for line in file:
            evtNum = line.split()[0]
            index = line.split()[1]
            layer = line.split()[2] + "x" + line.split()[3]
            track_eta = line.split()[4]
            track_phi = line.split()[5]
            extrap_eta = line.split()[6]
            extrap_phi = line.split()[7]
            pt = line.split()[8]

            if layer == selectedSurface:
                testv = evtNum + "x" + index + "x" + layer
                if testv in dict1:
                    print(testv)
                dict1[evtNum + "x" + index + "x" + layer] = [track_eta, track_phi, extrap_eta, extrap_phi, pt]

    file.close()
    print("Loaded file 2 to the array.")

    number_of_hits = len(dict1)
    print("Reference file - relevant lines: ", number_of_hits)
    print("Comparison file - relevant lines: ", len(arr1))


    from operator import itemgetter
    import math

		########## Comparing track hits ##########
    for item1 in arr1:
        if item1[1] == selectedSurface:
            key=item1[0] + "x" + item1[1]
            if key in dict1:
                data = dict1[key]
                del dict1[key]
                difference_eta = float(data[2]) - float(item1[4])
                difference_phi = float(data[3]) - float(item1[5])
                hist_eta.Fill(difference_eta)
                hist_phi.Fill(difference_phi)
                hist_eta_phi_distance.Fill(math.sqrt(difference_eta ** 2 + difference_phi ** 2))
                hist_eta_abs.Fill(abs(difference_eta))
                hist_phi_abs.Fill(abs(difference_phi))
                hist_eta_2D.Fill(difference_eta, float(data[4])/1000)
                hist_phi_2D.Fill(difference_phi, float(data[4])/1000)

    canvas1 = ROOT.TCanvas("Canvas1","c1",800,600)
    canvas1.cd()
    canvas1.SetLogy()
    hist_eta.Draw("")
    canvas1.Draw("hist")
    canvas1.Print("extrap_eta_delta" + selectedSurface + ".png")

    canvas2 = ROOT.TCanvas("Canvas2","c2",800,600)
    canvas2.cd()
    canvas2.SetLogy()
    hist_phi.Draw("")
    canvas2.Draw("hist")
    canvas2.Print("extrap_phi_delta" + selectedSurface + ".png")

    canvas3 = ROOT.TCanvas("Canvas3","c3",800,600)
    canvas3.cd()
    canvas3.SetLogy()
    hist_eta_abs.Draw("")
    canvas3.Draw("hist")
    canvas3.Print("extrap_eta_delta_abs" + selectedSurface + ".png")
        
    canvas4 = ROOT.TCanvas("Canvas4","c4",800,600)
    canvas4.cd()
    canvas4.SetLogy()
    hist_phi_abs.Draw("")
    canvas4.Draw("hist")
    canvas4.Print("extrap_phi_delta_abs" + selectedSurface + ".png")

    canvas5 = ROOT.TCanvas("Canvas5","c5",800,600)
    canvas5.cd()
    canvas5.SetLogz()
    hist_eta_2D.Draw("colz")
    canvas5.Draw("hist")
    canvas5.Print("extrap_eta_delta_2D" + selectedSurface + ".png")

    canvas6 = ROOT.TCanvas("Canvas6","c6",800,600)
    canvas6.cd()
    canvas6.SetLogz()
    hist_eta_2D.Draw("colz")
    canvas6.Draw("hist")
    canvas6.Print("extrap_phi_delta_2D" + selectedSurface + ".png")
    
    for key in dict1:
        data = dict1[key]
        track_pt = float(data[4])/1000
        track_eta = data[0]
        track_phi = data[1]
        hist_eta_pt.Fill(float(track_eta), float(track_pt))
        hist_phi_pt.Fill(float(track_phi), float(track_pt))
        hist_phi_eta.Fill(float(track_phi), float(track_eta))

    canvas7 = ROOT.TCanvas("Canvas7","c7",800,600)
    canvas7.cd()
    canvas7.SetLogz()
    hist_eta_pt.Draw("colz")
    canvas7.Draw("hist")
    canvas7.Print("missing_eta_pt_" + selectedSurface + ".png")

    canvas8 = ROOT.TCanvas("Canvas8","c8",800,600)
    canvas8.cd()
    canvas8.SetLogz()
    hist_phi_pt.Draw("colz")
    canvas8.Draw("hist")
    canvas8.Print("missing_phi_pt_" + selectedSurface + ".png")

    canvas9 = ROOT.TCanvas("Canvas9","c9",800,600)
    canvas9.cd()
    canvas9.SetLogz()
    hist_phi_eta.Draw("colz")
    canvas9.Draw("hist")
    canvas9.Print("missing_phi_eta_" + selectedSurface + ".png")
    
    canvas10 = ROOT.TCanvas("Canvas10","c10",800,600)
    canvas10.cd()
    canvas10.SetLogz()
    hist_eta_phi_distance.Draw("")
    canvas10.Draw("hist")
    canvas10.Print("extrap_phi_delta_distance" + selectedSurface + ".png")
    
    with open(file_comparison_precision, 'a') as output_1:
        output_1.write('\n' +  str(selectedLayer) + '    ' + str(hist_eta_phi_distance.GetMean()))
    output_1.close()
    
    with open(file_missing_hits, 'a') as output_2:
        if number_of_hits != 0:
            output_2.write('\n' +  str(selectedLayer) + '    ' + str(1 - (hist_phi_pt.GetEntries() / number_of_hits)))
        else:
            output_2.write('\n' +  str(selectedLayer) + '    ' + str(1 - (hist_phi_pt.GetEntries() / number_of_hits)))
    output_2.close()

########## Loop over calo layers ##########
for i in range (0, 13):
    compare_layer(i)
